# 
#  Copyright (c) 2013 Indian Institute of Technology, Madras. 
#  All Rights Reserved.

#  This program is free software. You can redistribute and/or modify
#  it in accordance with the terms of the accompanying license
#  agreement.  See LICENSE in the top-level directory for details.

for file in ../../tau2013_contest_benchmarks.v1.0/*
$i = 0
do
	echo $file
    python checkForGateToGate.py $file
    #whatever you need with "$file"
done
