# 
#  Copyright (c) 2013 Indian Institute of Technology, Madras. 
#  All Rights Reserved.

#  This program is free software. You can redistribute and/or modify
#  it in accordance with the terms of the accompanying license
#  agreement.  See LICENSE in the top-level directory for details.

i=0
for file in ../tau2013_contest_benchmarks.v1.0/*
do
    echo $file
    python checkForGateToGate.py $file $i.nodes
    ./myhash $i.nodes $i.hash
    #whatever you need with "$file"
    i=$((i+1))
done

