/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include "pins.h"
#include "main.h"
/*
 *
 * Template
 * --------
 * void add<node> (<node> *node, <list> *list)
 * {
 *    node->next = list->head;
 *    list->head = node;
 *    list->count++;
 * }
 *
 * void init<list> (<list> *list)
 * {
 *    list->head = NULL;
 *    list->count = 0;
 * }
 *
 * void free<node> (<node> *node)
 * {
 *    free (node);
 * }
 *
 * void free<list> (<list> *list)
 * {
 *
 *    <node> *node;
 *
 *    node = list->head;
 *    while(list->head != NULL)
 *    {
 *       list->head = node->next;
 *       free<node> ( node );
 *       node = list->head;
 *    }
 *
 * }
 *
*/

// Instance Input Pin
void addinstanceIPin (instanceIPin *node, IIPList *list)
{
   node->next = list->head;
   list->head = node;
   list->count++;
}

void initIIPList (IIPList *list)
{
   list->head = NULL;
   list->count = 0;
}

void freeinstanceIPin (instanceIPin *node)
{
   free (node);
}

void freeIIPList (IIPList *list)
{

   instanceIPin *node;

   node = list->head;
   while(list->head != NULL)
   {
      list->head = node->next;
      freeinstanceIPin ( node );
      node = list->head;
   }

}

// Instance Output Pin
void addinstanceOPin (instanceOPin *node, IOPList *list)
{
   node->next = list->head;
   list->head = node;
   list->count++;
}

void initIOPList (IOPList *list)
{
   list->head = NULL;
   list->count = 0;
}

void freeinstanceOPin (instanceOPin *node)
{
   free (node);
}

void freeIOPList (IOPList *list)
{

   instanceOPin *node;

   node = list->head;
   while(list->head != NULL)
   {
      list->head = node->next;
      freeinstanceOPin ( node );
      node = list->head;
   }

}

// Wire Port Pins
void addwirePort (wirePort *node, WPList *list)
{
   node->next = list->head;
   list->head = node;
   list->count++;
}

void initWPList (WPList *list)
{
   list->head = NULL;
   list->count = 0;
}

void freewirePort (wirePort *node)
{
   free (node);
}

void freeWPList (WPList *list)
{

   wirePort *node;

   node = list->head;
   while(list->head != NULL)
   {
      list->head = node->next;
      freewirePort ( node );
      node = list->head;
   }

}

// Wire Tap Pins
void addwireTap (wireTap *node, WTList *list)
{
   node->next = list->head;
   list->head = node;
   list->count++;
}

void initWTList (WTList *list)
{
   list->head = NULL;
   list->count = 0;
}

void freewireTap (wireTap *node)
{
   free (node);
}

void freeWTList (WTList *list)
{

   wireTap *node;

   node = list->head;
   while(list->head != NULL)
   {
      list->head = node->next;
      freewireTap ( node );
      node = list->head;
   }
}

#ifdef ENABLE_LOGGING
   void printTData(NODE_TIMING_DATA t, char *nodename, int dir)
   {
       if (dir == 0)
           fprintf(main_logfile, "%s\t%e\t%e\t%e\t%e\n", nodename, t.atRiseEarly[0], t.atRiseLate[0], t.atFallEarly[0], t.atFallLate[0]);
       else
           fprintf(main_logfile, "%s\t%e\t%e\t%e\t%e\n", nodename, t.ratRiseEarly[0], t.ratRiseLate[0], t.ratFallEarly[0], t.ratFallLate[0]);
   }
#endif
/* May be useful
void freeWNode (WIRE* node)
{
   int i;
   for ( i = 0; i < node->numtaps; i++)
   {
      free (node->tapnode[i]);
      //free (node->iotiming[i]);
   }
   free (node->tapnode);

   // Free RC tree
   for ( i = 0; i < node->rctree.numnodes; i++ )
   {
      free ( node->rctree.neighbour[i] );
      free ( node->rctree.res[i] );
   }
   free ( node->rctree.neighbour );
   free ( node->rctree.numneighbours );
   free ( node->rctree.cap );
   free ( node->rctree.res );
}
*/
