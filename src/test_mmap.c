/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include "timeit.h"

#define BUFFERSIZE  500

char *mygets(int num, char* in)
{
    static int i = 0;
    char *str;
    if(in[i] == '\0') return (char*)NULL;
    str = in + i;
    while(in[i]!='\n')
        i++;
    in[i] = '\0';
    i++;
    return str;
}

int main(int argc, char **argv)
{
    struct stat sb;
    stat(argv[1], &sb);
    int fd = open(argv[1], O_RDONLY), lno = 0;
    fprintf(stderr, "%ld\t", sb.st_size);
    char *memblk;
    TimeIt t;
    timeit_start(&t);
    memblk = mmap(NULL, sb.st_size, PROT_WRITE, MAP_PRIVATE, fd, 0);
    timeit_stop(&t);
    fprintf(stderr, "%ld\t", t.time_spent);
    timeit_start(&t);
    //fprintf(stderr, "%s", memblk);
    char *p = mygets(BUFFERSIZE, memblk);
    while(p != NULL)
    {
        //fprintf(stderr, "%d: %s\n", lno++, p);
        p = mygets(BUFFERSIZE, memblk);
    }
    timeit_stop(&t);
    fprintf(stderr, "%ld\n", t.time_spent);
    return 0;
}

