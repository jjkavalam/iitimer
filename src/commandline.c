/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

// Command line arguments following the first three are processed using getopts
// Reference: http://www.ibm.com/developerworks/aix/library/au-unix-getopt.html
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>

#include "main.h"

int process_args( int argc, char *const argv[] ) // Prototype borrowed from getopt
{

    static const char *optString = "p:m:h?";

    static const struct option longOpts[] = {
        { "num-parser-threads", required_argument, NULL, 'p' },
        { "message", required_argument, NULL, 'm' },
        { "help", no_argument, NULL, '?' },
        { "version", no_argument, NULL, 0 },
        { NULL, no_argument, NULL, 0 }
    };
        
    int longIndex, opt;
    
    opt = getopt_long( argc, argv, optString, longOpts, &longIndex );
    
    while (opt != -1)
    {
        switch (opt)
        {
            case 'p':
                global_params.netlist_parser_num_threads = atoi(optarg);
                break;
            case 'm':
                strcpy(global_params.debug_message,optarg);
                break;
            case 0:
                // For long options with no equivalent short option
                if (strcmp("version",longOpts[longIndex].name) == 0)
                {
                    printf("\nVersion 1.0.");
                }
                break;
            case '?':
                // Automatically gets evaluated when there is an unknown argument
                printf("\n -- Help -- \n");
                break;
            default:
                break;
        }
        opt = getopt_long( argc, argv, optString, longOpts, &longIndex );
    }
    return 1;
}
