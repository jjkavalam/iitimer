/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

/*
 *  SSTA Engine
 */

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "pins.h"
#include "parser_netlist.h"
#include "engine.h"
#include "vect.h"
#include "math.h"
#include "signal.h"
#include <assert.h>
#include "main.h"
#include "omp.h"
#include "pthread.h"

#define SQR(x) (x)*(x)
#define RT2 1.41421356237
#define GAUSS(x) 0.3989422804*exp(-(x*x)/2)
#define SQRT(x) (x>0)?sqrt(x):0

vector *Vv; // global vector for storing vectors containing level based instances
int revTimingFlag;
/* ****** QUEUE UTILS *************/

//int qSize, cellsProc; // global variable just for debug purpose remove afterwards

/*  void printq(queue *q){
    if(q!=NULL)
        fprintf(stderr,"Queue is not empty\n");
   //fprintf(stderr,"%d -> %d\n", q->ptr->index, (q->next==NULL)?-1:q->next->ptr->index);
}
*/

void initQ(queue *q, int size)
{
    q->qhead = -1;
    q->qtail = -1;
    q->size = size;
    q->ptr = malloc(size*sizeof(void*));
    if(q->ptr == NULL)
    {
        fprintf(stderr, "Couldn't allocate memory\n");
        exit(EXIT_FAILURE);
    }
    //fprintf(stderr, "Creating queue of size = %d\n", q->size);
}

void pushQ(queue *q, Instance *p)
{
    //fprintf(stderr, "Pushing to %d\n", q->qtail);
    if(q->qtail >= q->size)
    {
        fprintf(stderr, "Trying to access more than allocated\n");
        exit(EXIT_FAILURE);
    }
    if(q->qtail == -1)
    {
        q->qtail = q->qhead = 0;
        q->ptr[q->qtail] = p;
    }
    else
    {
        q->qtail++;
        q->ptr[q->qtail] = p;
    }
}

void popQ(queue* q)
{
    if(q->qhead <= q->qtail)
        q->qhead ++;
    else
        fprintf(stderr, "Trying to pop out from empty queue\n");
}

void destQ(queue *q)
{
    free(q->ptr);
}

/*********** QUEUE UTILS END ***************/

/***** UTILS FUNCTIONS FOR VECTORS *********/

    /* 0 -mean,
     * 1 - voltage, 2 - temp. 3 -threshold
     * 4 - L, 5 - W
     * 6 - metal
     * 7 - random component */

void COPY(float y[8], float x[8]){
    memcpy(y, x, 8*sizeof(float));
}

void copyatslew(NODE_TIMING_DATA *x, NODE_TIMING_DATA *y)
{
    COPY(x->atFallEarly, y->atFallEarly);
    COPY(x->atRiseEarly, y->atRiseEarly);
    COPY(x->atFallLate, y->atFallLate);
    COPY(x->atRiseLate, y->atRiseLate);

    COPY(x->slewFallEarly, y->slewFallEarly);
    COPY(x->slewRiseEarly, y->slewRiseEarly);
    COPY(x->slewFallLate, y->slewFallLate);
    COPY(x->slewRiseLate, y->slewRiseLate);
}

void copyrat(NODE_TIMING_DATA *x, NODE_TIMING_DATA *y)
{
    if(y->slack_early == 1)
    {
        COPY(x->ratFallEarly, y->ratFallEarly);
        COPY(x->ratRiseEarly, y->ratRiseEarly);
        x->slack_early = 1;
    }
    if(y->slack_late == 1)
    {
        COPY(x->ratFallLate, y->ratFallLate);
        COPY(x->ratRiseLate, y->ratRiseLate);
        x->slack_late = 1;
    }
}

void MAX(float c[8], float a[8], float b[8]){
    float vara=0, varb=0, rho=0;
    int i;
    for(i=1; i<8; i++){
        vara += SQR(a[i]);
        varb += SQR(b[i]);
        if(i<7) rho += a[i]*b[i]; // omit multiplication for random comp.
    }
    float theta = SQRT(vara + varb - 2*rho);
    if(theta == 0)
    {
	 if(a[0] > b[0]) COPY(c, a);
	 else COPY(c, b);
   	 return;
    } // for cases where a = b
    rho /= sqrt(vara*varb);                     /* calculating cor. coeff */
    //fprintf(stdout, "rho value : %f\n", rho);
    float alpha = (a[0] - b[0])/theta, ga, Ta;
    ga = GAUSS(alpha);
    Ta = (1+erf(alpha/RT2))/2;
    c[0] = a[0]*Ta + b[0]*(1-Ta)+theta*ga;
    float varc = (vara + SQR(a[0]))*Ta + (varb+SQR(b[0]))*(1-Ta)
               +  (a[0]+b[0])*theta*ga - SQR(c[0]);
    for(i=1; i<7; i++){
        c[i] = a[i]*Ta + b[i]*(1-Ta);
        varc -= SQR(c[i]);
    }
    c[7] = SQRT(varc);               /* CHECK THIS */
    //fprintf(stdout, "Random Part coeff : %e\n", c[7]);
    for(i=0; i<8; i++) if(c[i]!=c[i])   raise(SIGABRT);

}

void MIN(float c[8], float a[8], float b[8]){
    int i;
    for(i=0; i<8; i++) { a[i] = -a[i]; b[i] = -b[i]; }
    MAX(c, a, b);
    for(i=0; i<8; i++) {c[i] = -c[i]; a[i] = -a[i]; b[i] = -b[i];}
    // assuming arrays are always passed by reference
}

void ADD(float z[8], float x[8], float y[8]){
    int i;
    for(i=0; i<8; i++) z[i] = x[i]+y[i];
}

void SUB(float z[8], float x[8], float y[8]){
    int i; for(i=0; i<8; i++) z[i] = x[i] - y[i];
}

void computeSetupHold(float out[8], float dep[3], float sck[8], float sin[8])
{
    int i;
    out[0] = dep[0] + dep[1]*sck[0] + dep[2]*sin[0];
    for(i=1; i<=6; i++) out[i] = dep[1]*sck[i] + dep[2]*sin[i];
    out[7] = SQRT(SQR(sck[7]) + SQR(sin[7])); //random component
}

void computeParam(float out[8], float dep[9], float cap[2], float slew[8]){
    /*
     *  Compute the parametric delay / output-slew of a gate, given: (Ref: Sec 3.2)
     *      cell's inherent delay/slew in parametric form
     *      parametric output load capacitance
     *      parametric input slew
     */

    int i;

    // Delay, nominal = a + b*CL(nominal) + c*Si(nominal) [Eqn: 23]
    out[0] = dep[0] + dep[1]*cap[0] + dep[2]*slew[0];

    // D, sensitivity to X = a*k_d,x + c * s_v
    for(i=1; i<6; i++)
        out[i] = dep[0]*dep[i+2] + dep[2]*slew[i]; // general component

    // delay is sensitive to metal variation because it causes
    // the load capacitance to vary. Thus, sensitivity = b * lm
    out[6] = dep[1] * cap[1];

    // Delay, sensitivity to random variation
    out[7] = sqrt(SQR(dep[0]*dep[8])+SQR(dep[2]*slew[7]));

}
/***** UTILS FUNCTIONS END *****************/

void getTDataSense(NODE_TIMING_DATA *out, NODE_TIMING_DATA *nData, int pinsense, int dir)
{

    if(pinsense == POSITIVE_UNATE){ *out = *nData; return;}
    if(dir == FORWARD) {
        if(pinsense == NEGATIVE_UNATE){
            COPY(out->atRiseLate, nData->atFallLate);
            COPY(out->atFallLate, nData->atRiseLate);
            COPY(out->atRiseEarly, nData->atFallEarly);
            COPY(out->atFallEarly, nData->atRiseEarly);

            COPY(out->slewRiseLate, nData->slewFallLate);
            COPY(out->slewFallLate, nData->slewRiseLate);
            COPY(out->slewRiseEarly, nData->slewFallEarly);
            COPY(out->slewFallEarly, nData->slewRiseEarly);
        }
        else{
            float max[8], min[8];
            MAX(max, nData->atRiseLate, nData->atFallLate);
            MIN(min, nData->atRiseEarly, nData->atFallEarly);

            COPY(out->atRiseLate, max);
            COPY(out->atFallLate, max);
            COPY(out->atRiseEarly, min);
            COPY(out->atFallEarly, min);

            MAX(max, nData->slewRiseLate, nData->slewFallLate);
            MIN(min, nData->slewRiseEarly, nData->slewFallEarly);

            COPY(out->slewRiseLate, max);
            COPY(out->slewFallLate, max);
            COPY(out->slewRiseEarly, min);
            COPY(out->slewFallEarly, min);
        }
        return;
    }
    else
    {
        out->slack_early = nData->slack_early;
        out->slack_late = nData->slack_late;
        if(pinsense == NEGATIVE_UNATE)
        {
            COPY(out->ratRiseLate, nData->ratFallLate);
            COPY(out->ratFallLate, nData->ratRiseLate);
            COPY(out->ratRiseEarly, nData->ratFallEarly);
            COPY(out->ratFallEarly, nData->ratRiseEarly);
        }
        else
        {
            float max[8], min[8];
            MIN(min, nData->ratRiseLate, nData->ratFallLate);
            MAX(max, nData->ratRiseEarly, nData->ratFallEarly);

            COPY(out->ratRiseLate, min);
            COPY(out->ratFallLate, min);
            COPY(out->ratRiseEarly, max);
            COPY(out->ratFallEarly, max);
        }
    }
}

void calculateSlewOutput(float out[8], float in[8], float slewWire[2]){
    out[0] = sqrt(SQR(in[0]) + SQR(slewWire[0]));
    float so = slewWire[0];
    int i;
    for(i=1; i<=5; i++)
        // for all non-metal params
        // finite differencing
        out[i] = (sqrt(SQR(in[0]+3*in[i])+SQR(so))-
                sqrt(SQR(in[0]-3*in[i])+SQR(so)))/6;

    out[6] = (sqrt(SQR(in[0]+3*in[6])+SQR(so+3*slewWire[1]))-out[0])/3;
    out[7] = (sqrt(SQR(in[0]+3*in[7])+SQR(so))-
                sqrt(SQR(in[0]-3*in[7])+SQR(so)))/6;
   //  out[7] = in[7]; // assume no random component for wire
}

void addWireDelayParam(NODE_TIMING_DATA *out, NODE_TIMING_DATA *nodeT, WIRE_TIMING_DATA *w, int direction)
{
    if(direction == FORWARD)
    {
        COPY(out->atRiseLate, nodeT->atRiseLate);
        COPY(out->atRiseEarly, nodeT->atRiseEarly);
        COPY(out->atFallLate, nodeT->atFallLate);
        COPY(out->atFallEarly, nodeT->atFallEarly);

        out->atRiseLate[0] += w->risedelay[0]; // mean addition
        out->atRiseEarly[0] += w->risedelay[0];
        out->atFallLate[0] += w->falldelay[0];
        out->atFallEarly[0] += w->falldelay[0];

        out->atRiseLate[6] += w->risedelay[1]; // metal dependence
        out->atRiseEarly[6] += w->risedelay[1];
        out->atFallLate[6] += w->falldelay[1];
        out->atFallEarly[6] += w->falldelay[1];
        //delay calculation done!

        calculateSlewOutput(out->slewRiseLate, nodeT->slewRiseLate, w->riseslew);
        calculateSlewOutput(out->slewRiseEarly, nodeT->slewRiseEarly, w->riseslew);
        calculateSlewOutput(out->slewFallLate, nodeT->slewFallLate, w->fallslew);
        calculateSlewOutput(out->slewFallEarly, nodeT->slewFallEarly, w->fallslew);
        //slew calculation!
    }
    else if (direction == REVERSE)
    {
        COPY(out->ratRiseLate, nodeT->ratRiseLate);
        COPY(out->ratRiseEarly, nodeT->ratRiseEarly);
        COPY(out->ratFallLate, nodeT->ratFallLate);
        COPY(out->ratFallEarly, nodeT->ratFallEarly);

        out->ratRiseLate[0] -= w->risedelay[0]; // mean addition
        out->ratRiseEarly[0] -= w->risedelay[0];
        out->ratFallLate[0] -= w->falldelay[0];
        out->ratFallEarly[0] -= w->falldelay[0];

        out->ratRiseLate[6] -= w->risedelay[1]; // metal dependence
        out->ratRiseEarly[6] -= w->risedelay[1];
        out->ratFallLate[6] -= w->falldelay[1];
        out->ratFallEarly[6] -= w->falldelay[1];
    }
    else raise(SIGABRT);
}

#ifdef ENABLE_LOGGING
    void printParametric( float timing_quantity[8] )
    {
        fprintf( main_logfile, \
        "%.4e +  %.4eV + %.4eT + %.4eL + %.4eW + %.4eH + %.4eM + %.4eR", \
        timing_quantity[0], timing_quantity[1], timing_quantity[2], timing_quantity[3], timing_quantity[4], timing_quantity[5], timing_quantity[6], timing_quantity[7] \
        );
    }
    void printCellModel( float delay_params[9] )
    {
        fprintf( main_logfile, \
                "%.4e ( 1 + %.1fV + %.4fT + %.4fL + %.4fW + %.4fH + %.4fR ) + %.4fCL + %.4fSi", \
                delay_params[0], delay_params[3], delay_params[4], delay_params[5], delay_params[6], delay_params[7], delay_params[8], delay_params[1], delay_params[2] \
        );
    }
#endif

void addDelayParam(NODE_TIMING_DATA *out, NODE_TIMING_DATA *in, NODE_TIMING_DATA *in_sense, TIMING_DATA *t,
    float fallCap[2], float riseCap[2], int direction)
{

    float delay[8];
    computeParam(delay, t->falldelay, fallCap, in_sense->slewFallLate);
    if(direction == FORWARD)
        ADD(out->atFallLate, in->atFallLate, delay);
    else SUB(in->ratFallLate, out->ratFallLate, delay);

    computeParam(delay, t->falldelay, fallCap, in_sense->slewFallEarly);
    if(direction == FORWARD)
        ADD(out->atFallEarly, in->atFallEarly, delay);
    else SUB(in->ratFallEarly, out->ratFallEarly, delay);

    computeParam(delay, t->risedelay, riseCap, in_sense->slewRiseLate);
    if(direction == FORWARD)
        ADD(out->atRiseLate, in->atRiseLate, delay);
    else SUB(in->ratRiseLate, out->ratRiseLate, delay);

    computeParam(delay, t->risedelay, riseCap, in_sense->slewRiseEarly);
    if(direction == FORWARD)
        ADD(out->atRiseEarly, in->atRiseEarly, delay);
    else SUB(in->ratRiseEarly, out->ratRiseEarly, delay);

    if(direction == FORWARD)
    {
        computeParam(out->slewFallLate, t->fallslew, fallCap, in_sense->slewFallLate);
        computeParam(out->slewRiseLate, t->riseslew, riseCap, in_sense->slewRiseLate);
        computeParam(out->slewFallEarly, t->fallslew, fallCap, in_sense->slewFallEarly);
        computeParam(out->slewRiseEarly, t->riseslew, riseCap, in_sense->slewRiseEarly);
    }
    if(direction == REVERSE)
    {
       in->slack_early = out->slack_early;
       in->slack_late = out->slack_late;
    }
}


void forwardTiming(Instance* instance){
    /*  This is an instance for which the arrival times
     *  and delays need to be calculated in the following way
     *  Check the cell type and get delay-slew
     *  information from pin to pin with sense
     *  Get the delay-slew information at all pins at the input
     *  Ready to process.. Find all information at the output
     *  Propagate the output delay-slew to all fanout nodes
     *  Also get Load Capacitance (Fall/Rise) from wire*/

    /*  TIMING_DATA[i][j] = has coefficients for falldelay risedelay
     *  fallslew and riseslew for input pin i to output pin j*/
    if (instance->celltype == VSINK ) return;

    int i, j;
    P_CELL *cell = NULL; // P_CELL is a library cell

    if(instance->celltype != VSOURCE )
    {
        cell = &p_cell[instance->celltype];
        // fprintf(stderr, "Instance : %s\n", cell->name);
        #ifdef ENABLE_LOGGING
            fprintf( main_logfile, "\
                    \n\n Cell : %5s \
                    \n================", \
                    cell->name);
        #endif

    }
    else
    {
        #ifdef ENABLE_LOGGING
            fprintf( main_logfile, "\
                    \n\n Virtual Source \
                    \n================" );
        #endif

    }
    for(i = 0; i < instance->numOPins; i++){
        NODE_TIMING_DATA outTime;
        instanceOPin *iop = instance->opin[i];
        //if (iop==NULL) fprintf(stderr,"\n*Error: Instance's %d output pin is NULL .",i);
        // for each output pin calculate NODE_TIMING_DATA

        if (instance->celltype == VSOURCE)
        {
            outTime = instance->opin[i]->tData;
        }
        else
        {
            if (!cell->hasclkpin)
            for(j = 0; j < instance->numIPins; j++)
            {
                // compute arrival time from jth pin
                // to be computed atFallLate due to j, atRiseLate due to j
                NODE_TIMING_DATA outTemp, jTime, jIn;             /* Stores timing values at output due to jth input */
                TIMING_DATA *tData = &cell->iotiming[instance->ipin[j]->pinid][instance->opin[i]->pinid];
                getTDataSense(&jIn, &instance->ipin[j]->tData, tData->sense, FORWARD);
                addDelayParam(&jTime, &jIn, &jIn, tData, iop->fallCap, iop->riseCap, FORWARD);

                #ifdef ENABLE_LOGGING
                    // Equation 23
                    // delay = cell_delay + Cap + slew
                    //
                    // jTime = at + delay
                    fprintf( main_logfile, "\
                            \n Input : %5s ( %5s ) *Fall Late", \
                            cell->ipinname[instance->ipin[j]->pinid], instance->ipin[j]->nodename);
                    fprintf( main_logfile, "\
                            \n CL   = %.4e + %.4eM",\
                            iop->fallCap[0], iop->fallCap[1]);
                    fprintf( main_logfile, "\
                            \n at  = ");
                    printParametric(jIn.atFallLate);
                    fprintf( main_logfile, "\
                            \n D   = ");
                    printCellModel(tData.falldelay);
                    fprintf( main_logfile, "\
                            \n ==> at' = ");
                    printParametric(jTime.atFallLate);
                    fprintf( main_logfile, "\
                            \n Si = ");
                    printParametric(jIn.slewFallLate);
                    fprintf( main_logfile, "\
                            \n So  = ");
                    printCellModel(tData.fallslew);
                    fprintf( main_logfile, "\
                            \n ==> So' = ");
                    printParametric(jTime.slewFallLate);

                #endif

                // Now update output timing values
                if (j == 0)
                    outTime = jTime;
                else
                {
                    MAX(outTemp.atFallLate, outTime.atFallLate, jTime.atFallLate);
                    MIN(outTemp.atFallEarly, outTime.atFallEarly, jTime.atFallEarly);

                    MAX(outTemp.atRiseLate, outTime.atRiseLate, jTime.atRiseLate);
                    MIN(outTemp.atRiseEarly, outTime.atRiseEarly, jTime.atRiseEarly);

                    MAX(outTemp.slewFallLate, outTime.slewFallLate, jTime.slewFallLate);
                    MIN(outTemp.slewFallEarly, outTime.slewFallEarly, jTime.slewFallEarly);

                    MAX(outTemp.slewRiseLate, outTime.slewRiseLate, jTime.slewRiseLate);
                    MIN(outTemp.slewRiseEarly, outTime.slewRiseEarly, jTime.slewRiseEarly);
                    outTime = outTemp;
                }

            }
            // if instance has clkpin
            else {
                TIMING_DATA *tData = &cell->cotiming[instance->opin[i]->pinid];
                NODE_TIMING_DATA jIn;
                getTDataSense(&jIn, &instance->cpin->tData, tData->sense, FORWARD);
                // COPY(jIn.atRiseEarly, instance->cpin->tData.atRiseEarly);
                // COPY(jIn.atRiseLate, instance->cpin->tData.atRiseLate);
                // COPY(jIn.atFallEarly, instance->cpin->tData.atRiseEarly);
                // COPY(jIn.atFallLate, instance->cpin->tData.atRiseLate);

                // COPY(jIn.slewRiseEarly, instance->cpin->tData.slewRiseEarly);
                // COPY(jIn.slewRiseLate, instance->cpin->tData.slewRiseLate);
                // COPY(jIn.slewFallEarly, instance->cpin->tData.slewRiseEarly);
                // COPY(jIn.slewFallLate, instance->cpin->tData.slewRiseLate);

                addDelayParam(&outTime, &jIn, &jIn, tData, iop->fallCap, iop->riseCap, FORWARD);
            }
            // update NODE_TIMING_DATA of output
            copyatslew(&instance->opin[i]->tData, &outTime);
        }
        // propagate the output delay through wires to gate input pins
        // but, in case of a direct connection propagate directly to the gate input pins

//        printTData(outTime, instance->opin[i]->nodename, 0);

        #ifdef ENABLE_LOGGING
            // at_out = MAX ( .. at'.. )
            fprintf( main_logfile, "\
                    \n\n Output : %5s ( %5s ) *fall late", \
                    cell->opinname[iop->pinid],iop->nodename);
            fprintf( main_logfile, "\
                    \n ==> MAX{at'}   = ");
            printParametric(outTime.atFallLate);
            fprintf( main_logfile, "\
                    \n ==> MAX{slew'} = ");
            printParametric(outTime.slewFallLate);
            fprintf( main_logfile, " \
                    \n\n");
        #endif

        if (instance->opin[i]->iip != NULL)
        {
            instance->opin[i]->iip->tData = outTime;

            #ifdef ENABLE_LOGGING
                fprintf ( main_logfile, " \
                        \n Copy the timing data to %5s\n\n", \
                        instance->opin[i]->iip->nodename);
            #endif
        }
        else if (instance->opin[i]->wp != NULL)
        {
            Wire* wireOp = instance->opin[i]->wp->wire;
            //assert(wireOp->numTaps < MAX_TAPS);
            #ifdef ENABLE_LOGGING
                fprintf ( main_logfile, " \
                        \n Propage through wire port %5s", \
                        instance->opin[i]->wp->nodename);
            #endif

            for(j = 0; j < wireOp->numTaps; j++){
                NODE_TIMING_DATA temp;
                addWireDelayParam(&temp, &outTime, &wireOp->iotiming[j], FORWARD);
                // update node timing data for input pin of instance
                copyatslew(&wireOp->tap[j]->iip->tData, &temp);
//                printTData(temp, wireOp->tap[j]->iip->nodename, FORWARD);
                #ifdef ENABLE_LOGGING
                    // at_out = MAX ( .. at'.. )
                    fprintf( main_logfile, "\
                            \n Tap %2d : %5s ", \
                            j, wireOp->tap[j]->iip->nodename);
                    fprintf( main_logfile, "\
                            \n ==> at  = ");
                    printParametric(temp.atFallLate);
                    fprintf( main_logfile, "\
                            \n ==> slew= ");
                    printParametric(temp.slewFallLate);
                #endif

            }

        }
    }
}

void backTiming(Instance *instance)
{
    // change backtiming to start from each wire at the output calculate value
    // at the port of the wire and then propagate to the input
    // no issues of 2 inputs because all combo blocks have only one output

    if (instance->celltype == VSINK ) return;

    int i, j, ct = instance->celltype, hcp = 0;
    P_CELL *cell; // P_CELL is a library cell

    for(i = 0; i < instance->numOPins; i++)
    {
        // calculate rat at the output based on the rats at the wire ports
        instanceOPin *iop = instance->opin[i];
        if (iop->wp == NULL) continue;
        Wire *w = iop->wp->wire;
        NODE_TIMING_DATA *temp = &(iop->tData);
        for(j = 0; j < w->numTaps; j++)
        {
            // for each tap find the corresponding delay at the port
            // and then update depending on value set at the taps
            instanceIPin *iip = w->tap[j]->iip;
            NODE_TIMING_DATA inTime, inTemp = iop->tData;
            // inTime stores the rat(tapnode)-wiredelay
            if(temp->slack_early == 1 || temp->slack_late == 1)
                // don't do this calculation if slacks are not set
                // because then rats can be directly copied
                addWireDelayParam(&inTime, &iip->tData, &w->iotiming[j], REVERSE);

            if(iip->tData.slack_early == 1)
            {
                if (temp->slack_early != 1)
                {
                    temp->slack_early = 1;
                    addWireDelayParam(temp, &iip->tData, &w->iotiming[j], REVERSE);
                }
                else
                {
                    MAX(temp->ratFallEarly, inTime.ratFallEarly, inTemp.ratFallEarly);
                    MAX(temp->ratRiseEarly, inTime.ratRiseEarly, inTemp.ratRiseEarly);
                }
            }

            if(iip->tData.slack_late == 1)
            {
                if (temp->slack_late != 1)
                {
                    temp->slack_late = 1;
                    addWireDelayParam(temp, &iip->tData, &w->iotiming[j], REVERSE);
                }
                else
                {
                    MIN(temp->ratFallLate, inTime.ratFallLate, inTemp.ratFallLate);
                    MIN(temp->ratRiseLate, inTime.ratRiseLate, inTemp.ratRiseLate);
                }
            }
        }
    }
    // calculated rat values at the output of instance!
    // now propagate directly to the input of the gate
    // stop the process here if it is a vsource
    // fprintf(stderr, "Output nodename %s\t", instance->opin[0]->nodename);

    if(instance->celltype == VSOURCE)
        return;
    if(revTimingFlag == 0) return;

    // will not come here if it was VSOURCE or VSINK
    assert(instance->celltype != VSOURCE && instance->celltype != VSINK);
    cell = &p_cell[instance->celltype];

    for(i = 0; i < instance->numIPins; i++)
    {
        NODE_TIMING_DATA inTime;
        if(!cell->hasclkpin)
        {
            for( j = 0; j < instance->numOPins; j++)
            {
                // hopefully only one pin is there
                NODE_TIMING_DATA inTemp, inSense, jTime, jOut = instance->ipin[i]->tData;             /* Stores timing values at output due to jth input */
                TIMING_DATA *tData = &cell->iotiming[instance->ipin[i]->pinid][instance->opin[j]->pinid];
                instanceOPin *iop = instance->opin[j];
                if(iop->tData.slack_early != 1 && iop->tData.slack_late != 1)
                    continue;

                // made sure both addDelayParam and getTDataSense carry slack flags
                getTDataSense(&inSense, &jOut, tData->sense, FORWARD);
                addDelayParam(&iop->tData, &jOut, &inSense, tData, iop->fallCap, iop->riseCap, REVERSE);
                getTDataSense(&jTime, &jOut, tData->sense, REVERSE);
                //fprintf(stdout, "Out Node : %s In Node : %s\n", instance->opin[i]->nodename, instance->ipin[j]->nodename);
                // Now update output timing values
                if (j == 0)
                    copyrat(&inTime, &jTime);
                else
                {
                    // thanks to HA_X1 we come here....
                    // fprintf(stderr, "Something fishy, no cell has 2 opins without clock\n");
                    // raise(SIGABRT);
                    MIN(inTemp.ratFallLate, inTime.ratFallLate, jTime.ratFallLate);
                    MAX(inTemp.ratFallEarly, inTime.ratFallEarly, jTime.ratFallEarly);

                    MIN(inTemp.ratRiseLate, inTime.ratRiseLate, jTime.ratRiseLate);
                    MAX(inTemp.ratRiseEarly, inTime.ratRiseEarly, jTime.ratRiseEarly);
                    copyrat(&inTime, &inTemp);
                }
            }
            copyrat(&instance->ipin[i]->tData, &inTime);
        }
        else
        {
            /*
             * tsetup = g + h * Sck + j * Si // separate for rise and fall
             * thold  = m + n * Sck + p * Si
             * ratlate = T + lolate - tsetup // want even atlate to come before setup
             * ratearly = loearly + thold // want atearly to come after the hold constraint
             *  */
            // Need to add clock signals based elements here also
            // late arrival time and early arrival time for each pin -> ratRiseLate and ratRiseEarly
            // copy from atRiseLate and atRiseEarly
            // and make slack 1
            NODE_TIMING_DATA *inPtr = &instance->ipin[i]->tData;
            float tq[8];
            if(cell->setup[instance->ipin[i]->pinid].hasdata != 1) continue;
            // rise
            // setup
            computeSetupHold(tq, cell->setup[instance->ipin[i]->pinid].rise, instance->cpin->tData.slewRiseEarly, inPtr->slewRiseLate);
            SUB(inPtr->ratRiseLate, instance->cpin->tData.atRiseEarly, tq);
            inPtr->ratRiseLate[0] += clockTP; // rat late calculation
            // fprintf(stderr, "Rise %s : %e %e\n", instance->ipin[i]->nodename, inPtr->ratRiseLate[0], inPtr->atRiseLate[0]);

            // hold
            computeSetupHold(tq, cell->hold[instance->ipin[i]->pinid].rise, instance->cpin->tData.slewRiseLate, inPtr->slewRiseEarly);
            ADD(inPtr->ratRiseEarly, instance->cpin->tData.atRiseLate, tq);
            // fall
            computeSetupHold(tq, cell->setup[instance->ipin[i]->pinid].fall, instance->cpin->tData.slewRiseEarly, inPtr->slewFallLate);
            SUB(inPtr->ratFallLate, instance->cpin->tData.atRiseEarly, tq);
            inPtr->ratFallLate[0] += clockTP; // rat late calculation
            // fprintf(stderr, "Fall %s : %e %e\n", instance->ipin[i]->nodename, inPtr->ratFallLate[0], inPtr->atFallLate[0]);

            // hold
            computeSetupHold(tq, cell->hold[instance->ipin[i]->pinid].fall, instance->cpin->tData.slewRiseLate, inPtr->slewFallEarly);
            ADD(inPtr->ratFallEarly, instance->cpin->tData.atRiseLate, tq);
            inPtr->slack_early = 1;
            inPtr->slack_late = 1;

        }
    }
}

int max_int(int x, int y){ return (x>y)?x:y; }
int min_int(int x, int y){ return (x<y)?x:y; }

pthread_barrier_t barr;
void* forwTimingParallel(void *p)
{
    int i, j, levels = Vv->size, tid =(int) p;
    for(i = 0; i < levels; i++)
    {
        vector *V = (vector*) Vv->ptr[i];
        int chunk = max_int(V->size/num_threads+1, 8);
        for(j = tid*chunk; j < min_int(V->size, (tid+1)*chunk); j++)
        {
            forwardTiming((Instance*)V->ptr[j]);
            //fprintf(stderr, "Called from thread %d in level %d\n", tid, i);
        }
        int rc = pthread_barrier_wait(&barr);
        // here deallocation of V can happen!!
        if(tid == 0) destroy_vect(V);
        // fprintf(stderr, "Called from thread %d barrier %p\n", tid, p);
        if(rc != 0 && rc != PTHREAD_BARRIER_SERIAL_THREAD)
        {
            fprintf(stderr, "Could not wait on barrier\n");
            exit(-1);
        }
    }
    return NULL;
}

void* revTimingParallel(void *p)
{
    int i, j, levels = Vv->size, tid =(int) p;
    for(i = 0; i < levels; i++)
    {
        vector *V = (vector*) Vv->ptr[i];
        int chunk = max_int(V->size/num_threads+1, 8);
        for(j = tid*chunk; j < min_int(V->size, (tid+1)*chunk); j++)
        {
            backTiming((Instance*)V->ptr[j]);
            //fprintf(stderr, "Called from thread %d in level %d\n", tid, i);
        }
        int rc = pthread_barrier_wait(&barr);
        // here deallocation of V can happen!!
        //if(tid == 0) destroy_vect(V);
        //fprintf(stderr, "\n");
        if(rc != 0 && rc != PTHREAD_BARRIER_SERIAL_THREAD)
        {
            fprintf(stderr, "Could not wait on barrier\n");
            exit(-1);
        }
    }
    return NULL;
}

void timingAnalysis(){


    // Doing Timing Analysis
    #ifdef ENABLE_LOGGING
        fprintf(main_logfile, "Timing Analysis Started\n");
        fprintf(main_logfile, "Nodename\tEarly Rise\tLate Rise\tEarly Fall\tLate Fall\n");
    #endif
    int i;

    // V, Vprev act as the queues of each level; Vv is vec of vectors
    vector *Vprev, *V, *Vback;
    Vv = malloc(sizeof(vector));
    initV(Vv, 32);

    V = malloc(sizeof(vector));
    initV(V, 1);

    Vback = malloc(sizeof(vector));
    initV(Vback, 1);

    pushbackV(V, vSource);
    /*  Timing Analysis Starts Here!*/

    int cellsProc_forward = 0;
    while(V->size != 0)
    {
        pushbackV(Vv, V);
        //fprintf(stderr, "Number of elements in V = %d\n", V->size);
        int itr = 0;
        Vprev = V;
        V = malloc(sizeof(vector));
        initV(V, 1);
        for(itr = 0; itr < Vprev->size; itr++)
        {
            Instance* top = (Instance*)Vprev->ptr[itr];
            if (top->celltype >= 0)
            {
                // if instance has clkpin then add it to backtiming queue
                // fprintf(stderr, "\nProcessing : %s\t", p_cell[top->celltype].name);
                if (p_cell[top->celltype].hasclkpin == 1)
                {
                    pushbackV(Vback, top);
                    // top->cpin->tData.slack_early = 1;
                    // top->cpin->tData.slack_late = 1;
                }
            }//forwardTiming(top);
            for(i=0; i<top->numOPins; i++){
                Instance *child;
                instanceOPin *op = top->opin[i];
                //fprintf(stderr, "Output Pin : %s\t", op->nodename);
                //if (op == NULL) fprintf(stderr,"\n*Error: Instance's %d output pin is NULL .",i);
                if(op->iip!=NULL){
                    child = op->iip->instance; // o/p directly conn to i/p
                    //if (child == NULL) fprintf(stderr,"\n*Error: This gate input pin doesn't point to a valid Instance .");
                    child->numVisIn++;
                    if(child->numVisIn == child->numIPins){
                        if(child->celltype >= 0)
                        {    if(p_cell[child->celltype].hasclkpin != 1)
                                pushbackV(V, child);
                        }
                        else
                            pushbackV(V, child);
                    }
                }
                else if(op->wp != NULL)
                {
                    Wire *w = op->wp->wire;
                    //if (w == NULL) fprintf(stderr,"\n*Error: This wire port pin doesn't point to a valid wire .");
                    int itr;
                    for(itr =0; itr < w->numTaps; itr++)
                    {
                        //if (w->tap[itr]->iip == NULL) fprintf(stderr,"\n*Error: Wire's tap pin is not connected to a gate input.");
                        child = w->tap[itr]->iip->instance;
                        //if (child == NULL) fprintf(stderr,"\n*Error: This gate input pin doesn't point to a valid Instance .");
                        if (w->tap[itr]->iip->isclock ){
                            pushbackV(V, child);
                        }
                        else {
                            child->numVisIn++;
                            if(child->numVisIn == child->numIPins){
                                // check if this is a ff. if not don't add
                                if(child->celltype >= 0)
                                    if(!p_cell[child->celltype].hasclkpin)
                                    pushbackV(V, child);
                            }
                        }
                    }
                }
                else
                {
                    //fprintf(stderr,"\n*Error: Instance's output pin is not connected to a gate input or a wire port.");
                }
            }
        }
        //fprintf(stdout, "Forward: Cells Processed : %d in thread %d\n", ++cellsProc_forward, omp_get_thread_num());
    }

    pthread_t thr[num_threads];
    pthread_barrier_init(&barr, NULL, num_threads);
    int rcc;
    for(i = 0; i < num_threads; i++)
        rcc = pthread_create(&thr[i], NULL, forwTimingParallel, (void*)i);
    for(i = 0; i < num_threads; i++)
        rcc = pthread_join(thr[i], NULL);


    // fprintf(stderr,"Forward Timing Analysis Finished\n");
    destroy_vect(Vv);
    //return;
    revTimingFlag = 1;

    initV(Vv, 32);// for rev timing
    pushbackV(Vback, vSink);
    V = Vback;

    int cellsProc_back = 0;
    while(V->size != 0)
    {
        pushbackV(Vv, V);

        int itr = 0;
        Vprev = V;
        V = malloc(sizeof(vector));
        initV(V, 1);
        for(itr = 0; itr < Vprev->size; itr++)
        {
            Instance *top = Vprev->ptr[itr];
            //fprintf(stderr, "%d ", )
            //backTiming(top);

            for(i=0; i<top->numIPins; i++)
            {
                Instance *parent;
                instanceIPin *ip = top->ipin[i];
                //fprintf(stderr, "%s\n", ip->nodename);
                if(ip->iop != NULL)
                {
                    parent = ip->iop->instance;
                    parent->numVisOut++;
                    if(parent->numVisOut == parent->numFanOut)
                        if(parent->celltype >= 0)
                            if(!p_cell[parent->celltype].hasclkpin)
                            pushbackV(V, parent);
                }
                else if(ip->wt != NULL)
                {
                    parent = ip->wt->wire->port->iop->instance;
                    parent->numVisOut++;
                    if(parent->numVisOut == parent->numFanOut)
                        if(parent->celltype >= 0)
                            if(!p_cell[parent->celltype].hasclkpin)
                                pushbackV(V, parent);
                }

            }
            //fprintf(stdout, " Cells Processed : %d\n", ++cellsProc_back);
        }
    }

    for(i = 0; i < num_threads; i++)
        rcc = pthread_create(&thr[i], NULL, revTimingParallel, (void*)i);
    for(i = 0; i < num_threads; i++)
        rcc = pthread_join(thr[i], NULL);
    revTimingFlag = 0;
    for(i = 0; i<Vback->size; i++)
        backTiming(Vback->ptr[i]);
    backTiming(vSource);

    destroy_vect(Vv);
    fprintf(stderr, "Results can be found in %s\n", outFile);

}
