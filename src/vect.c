/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "vect.h"

void initV(vector *v, int capacity)
{
    v->size = 0;
    v->capacity = capacity;
    v->ptr = malloc(v->capacity*sizeof(void*));
    if(v->ptr == NULL)
    {
        //fprintf(stderr, "Memory allocation failed\n");
        exit(EXIT_FAILURE);
    }
}

void pushbackV(vector *v, void *p)
{
    // Pushes back on to the back of the vector
    // Increases capacity if necessary
    v->size = v->size + 1;
    if (v->size > v->capacity)
    {
        v->capacity = v->capacity << 1;                          /* Double the capacity */
        v->ptr = realloc(v->ptr, v->capacity*sizeof(void*));
    }
    v->ptr[v->size-1] = p;                         /* Store p in sizeth position */
}

void *popbackV(vector *v)
{
    // Pop out from the back of the vectory.
    // Not really useful as of now.
    v->size = v->size - 1;
    if(v->size > 0)
        return v->ptr[v->size];
    else
    {
        //fprintf(stderr, "Tried popping out an empty vector!\n");
        exit(EXIT_FAILURE);
    }
}

void *popV(vector *v)
{
    v->size = v->size - 1;
    v->capacity = v->capacity - 1;
    if(v->size > 0)
    {
       v->ptr = v->ptr + 1;
       return v->ptr[0];
    }
    else
    {
        //fprintf(stderr, "Tried popping out an empty vector!\n");
        exit(EXIT_FAILURE);
    }
}

int nearPowOf2(int n)
{
    n--;
    n |= n >> 1;
    n |= n >> 2;
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;
    n++;
    return n;
}

void addV(vector *dest, vector *source)
{
    // idea is to find new size, nearest capacity; realloc to that
    // ignore any previous definition
    int newSize = dest->size + source->size;
    dest->capacity = nearPowOf2(newSize);
    dest->ptr = realloc(dest->ptr, dest->capacity*sizeof(void*));
    memcpy(dest->ptr + dest->size, source->ptr, source->size *sizeof(void*));
    dest->size = newSize;
}

void destroy_vect(vector *v)
{
    v->size = 0;
    v->capacity = 0;
    free(v->ptr);
    return;    
}
