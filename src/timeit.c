/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#include "timeit.h"

/*    char buffer[30];
    struct timeval tv;    
    time_t curtime;
    gettimeofday(&tv, NULL); 
    curtime=tv.tv_sec;    
    strftime(buffer,30,"%m-%d-%Y  %T.",localtime(&curtime));
    printf("%s%ld\n",buffer,tv.tv_usec);
*/

void timeit_start(TimeIt *myclock)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    myclock->begin = tv;
}
void timeit_stop(TimeIt *myclock)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);    
    myclock->end = tv;
        
    myclock->time_spent = (myclock->end.tv_sec-myclock->begin.tv_sec)*1000000 + myclock->end.tv_usec-myclock->begin.tv_usec;
}
