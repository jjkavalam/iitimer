/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#include "hash.h"
#include "pins.h"
#include <stdlib.h>
#include <stdio.h>
#include "main.h"
#include "timeit.h"

#define HASH_BASED
unsigned int hash( const char* s, unsigned int seed)
{
    unsigned hash = seed;
    while (*s)
    {
        hash = hash * 101  +  *s++;
    }
    return hash;
}

unsigned int *iipHashKey;
instanceIPin **iipHashNode;
void createiipHash()
{
    int i;

    instanceIPin *node;
    unsigned int hashVal;

    iipHashKey = (unsigned int*)malloc(sizeof(unsigned int)*iipList.count);
    iipHashNode = (instanceIPin**)malloc(sizeof(instanceIPin*)*iipList.count);
    node = iipList.head;

    i = 0;
    while(node != NULL)
    {
       hashVal = hash (node->nodename,0);
       iipHashKey[i] = hashVal;
       iipHashNode[i] = node;
       node = node->next;
       i++;
    }

}

unsigned int *iopHashKey;
instanceOPin **iopHashNode;
void createiopHash()
{
    int i;

    instanceOPin *node;
    unsigned int hashVal;

    iopHashKey = (unsigned int*)malloc(sizeof(unsigned int)*iopList.count);
    iopHashNode = (instanceOPin**)malloc(sizeof(instanceOPin*)*iopList.count);
    node = iopList.head;

    i = 0;
    while(node != NULL)
    {
       hashVal = hash (node->nodename,0);
       iopHashKey[i] = hashVal;
       iopHashNode[i] = node;
       node = node->next;
       i++;
    }

}


#ifndef HASH_BASED
    unsigned int *wtHashKey;
    wireTap **wtHashNode;
    void createwtHash()
    {
        int i;

        wireTap *node;
        unsigned int hashVal;

        wtHashKey = (unsigned int*)malloc(sizeof(unsigned int)*wtList.count);
        wtHashNode = (wireTap**)malloc(sizeof(wireTap*)*wtList.count);
        node = wtList.head;

        i = 0;
        while(node != NULL)
        {
           hashVal = hash (node->nodename,0);
           wtHashKey[i] = hashVal;
           wtHashNode[i] = node;
           node = node->next;
           i++;
        }

    }

    struct _wireTap* searchWTNode(char* nodename)
    {
        int i;

        unsigned int key = hash (nodename,0);
        wireTap *WTNode;

        for ( i = 0; i < wtList.count; i++ )
        {
           if ( key == wtHashKey[i] ) break;
        }
        if ( i < wtList.count )
        {
            WTNode = wtHashNode[i];
            return WTNode;
        }
        return NULL;
    }

unsigned int *wpHashKey;
wirePort **wpHashNode;
void createwpHash()
{
    int i;

    wirePort *node;
    unsigned int hashVal;

    wpHashKey = (unsigned int*)malloc(sizeof(unsigned int)*wpList.count);
    wpHashNode = (wirePort**)malloc(sizeof(wirePort*)*wpList.count);
    node = wpList.head;

    i = 0;
    while(node != NULL)
    {
       hashVal = hash (node->nodename,0);
       wpHashKey[i] = hashVal;
       wpHashNode[i] = node;
       node = node->next;
       i++;
    }

}

struct _wirePort* searchWPNode(char* nodename)
{
    int i;

    unsigned int key = hash (nodename,0);
    wirePort *WPNode;

    for ( i = 0; i < wpList.count; i++ )
    {
       if ( key == wpHashKey[i] ) break;
    }
    if ( i < wpList.count )
    {
        WPNode = wpHashNode[i];
        return WPNode;
    }
    return NULL;
}

#else
    wireTHash *wTHash;

    void createwtHash()
    {
        int i;

        wireTap *node;
        unsigned int hashVal;

    /*     wtHashKey = (unsigned int*)malloc(sizeof(unsigned int)*wtList.count);
     *     wtHashNode = (wireTap**)malloc(sizeof(wireTap*)*wtList.count);
     */
        wTHash = malloc(sizeof(wireTHash)*wtList.count);
        node = wtList.head;

        i = 0;
        while(node != NULL)
        {
           hashVal = hash (node->nodename,0);
           wTHash[i].hash = hashVal;
           wTHash[i].node = node;
           node = node->next;
           i++;
        }

        //for(i=0; i<wtList.count; i++) //fprintf(stdout, "%u ", wTHash[i].hash);
        // printf("\n");
        #ifdef ENABLE_TIMEIT
            TimeIt time_sortWTHash;
            timeit_start(&time_sortWTHash);
        #endif

        sortWTHash(0, i-1);
        #ifdef ENABLE_TIMEIT
            timeit_stop(&time_sortWTHash);
            //fprintf(perf_logfile, "\n\t sortWTHash (N = %d): %ld", i-1,time_sortWTHash.time_spent);
        #endif

        //for(i=0; i<wtList.count; i++) //fprintf(stdout, "%u ", wTHash[i].hash);

    }

    struct _wireTap* searchWTNode(char* nodename)
    {
        int i;

        unsigned int key = hash (nodename,0);
        int lo = 0, hi = wtList.count -1, mid;
        while(lo <= hi){
            mid = (lo + hi)/2;
            if(wTHash[mid].hash == key) return wTHash[mid].node;
            else if(wTHash[mid].hash < key) lo = mid + 1;
            else hi = mid - 1;
        }

        return NULL;
    }

    void swapWTHash(int i, int j){
        if(j < i) return;
        wireTHash temp;
        temp = wTHash[i];
        wTHash[i] = wTHash[j];
        wTHash[j] = temp;
    }

    void sortWTHash(int start, int end){
        // Quick sort
        if(start >= end) return;
        swapWTHash(start, (start + end)/2);
        unsigned int pivot = wTHash[start].hash;
        int itr, count = 1;
        for(itr = start + 1; itr <= end; itr++)
        {
            if(wTHash[itr].hash < pivot) {
                swapWTHash(start + count, itr);
                count ++;
            }
        }
        // swap pivot element and just the element before count
        swapWTHash(start, start + count - 1);
        // sort from start to start to start + count -2
        sortWTHash(start, start + count - 2);
        // sort the remaining
        sortWTHash(start + count, end);
    }


    wirePHash *wPHash;

    void createwpHash()
    {
        int i;

        wirePort *node;
        unsigned int hashVal;

    /*     wtHashKey = (unsigned int*)malloc(sizeof(unsigned int)*wtList.count);
     *     wtHashNode = (wireTap**)malloc(sizeof(wireTap*)*wtList.count);
     */
        wPHash = malloc(sizeof(wirePHash)*wpList.count);
        node = wpList.head;

        i = 0;
        while(node != NULL)
        {
           hashVal = hash (node->nodename,0);
           wPHash[i].hash = hashVal;
           wPHash[i].node = node;
           node = node->next;
           i++;
        }

        //for(i=0; i<wtList.count; i++) //fprintf(stdout, "%u ", wTHash[i].hash);
        // printf("\n");
        #ifdef ENABLE_TIMEIT
            TimeIt time_sortWPHash;
            timeit_start(&time_sortWPHash);
        #endif

        sortWPHash(0, i-1);
        #ifdef ENABLE_TIMEIT
            timeit_stop(&time_sortWPHash);
            //fprintf(perf_logfile, "\n\t sortWPHash (N = %d): %ld", i-1,time_sortWPHash.time_spent);
        #endif

        //for(i=0; i<wtList.count; i++) //fprintf(stdout, "%u ", wTHash[i].hash);

    }

    struct _wirePort* searchWPNode(char* nodename)
    {
        int i;

        unsigned int key = hash (nodename,0);
        int lo = 0, hi = wpList.count -1, mid;
        while(lo <= hi){
            mid = (lo + hi)/2;
            if(wPHash[mid].hash == key) return wPHash[mid].node;
            else if(wPHash[mid].hash < key) lo = mid + 1;
            else hi = mid - 1;
        }

        return NULL;
    }

    void swapWPHash(int i, int j){
        if(j < i) return;
        wirePHash temp;
        temp = wPHash[i];
        wPHash[i] = wPHash[j];
        wPHash[j] = temp;
    }

    void sortWPHash(int start, int end){
        // Quick sort
        if(start >= end) return;
        swapWPHash(start, (start + end)/2);
        unsigned int pivot = wPHash[start].hash;
        int itr, count = 1;
        for(itr = start + 1; itr <= end; itr++)
        {
            if(wPHash[itr].hash < pivot) {
                swapWPHash(start + count, itr);
                count ++;
            }
        }
        // swap pivot element and just the element before count
        swapWPHash(start, start + count - 1);
        // sort from start to start to start + count -2
        sortWPHash(start, start + count - 2);
        // sort the remaining
        sortWPHash(start + count, end);
    }
#endif



struct _instanceIPin* searchIIPNode(char* nodename)
{
    int i;

    unsigned int key = hash (nodename,0);
    instanceIPin *IIPNode;

    for ( i = 0; i < iipList.count; i++ )
    {
       if ( key == iipHashKey[i] ) break;
    }
    if ( i < iipList.count )
    {
        IIPNode = iipHashNode[i];
        return IIPNode;
    }
    return NULL;
}

struct _instanceOPin* searchIOPNode(char* nodename)
{
    int i;

    unsigned int key = hash (nodename,0);
    instanceOPin *IOPNode;

    for ( i = 0; i < iopList.count; i++ )
    {
       if ( key == iopHashKey[i] ) break;
    }
    if ( i < iopList.count )
    {
        IOPNode = iopHashNode[i];
        return IOPNode;
    }
    return NULL;
}


