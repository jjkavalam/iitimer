/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

/*
// Example to illustrate parallelization techniques using openmp
#include <stdio.h>
#include <omp.h>

// Example 1
int main(int argc, char *argv[]) {
  int iam = 0, np = 1;

  #pragma omp parallel default(shared) private(iam, np)
  {
    #if defined (_OPENMP)
      // The two omp_ functions below belong to the so called "run time"
      // environment of OpenMP. However, as such parallelizing a piece of code
      // using OpenMP doesn't require them. It can be done using #pragma
      np = omp_get_num_threads();
      iam = omp_get_thread_num();
    #endif
    printf("Hello from thread %d out of %d\n", iam, np);
  }
}
*/


// Example 2
// Needs to figure out why uncommenting the #pragma below doesn't give any
// significant speedup
#include <stdio.h>
#include <math.h>
#include <timeit.h>
#include <stdlib.h>
#include <omp.h>
int main()
{
    int i;
    long N = pow(2,20);

    int* A = (int*)malloc(sizeof(int)*N);
    int* B = (int*)malloc(sizeof(int)*N);
    int* C = (int*)malloc(sizeof(int)*N);
    srand(time(NULL));

    TimeIt time_loop;

    timeit_start(&time_loop);
    #pragma omp parallel for default(shared) private(i)

        for ( i = 0; i < N; i++)
        {
            C[i] = A[i] + B[i];
        }

    timeit_stop(&time_loop);
    printf("The loop took %ld microseconds.\n",time_loop.time_spent);
    timeit_start(&time_loop);

    for ( i = 0; i < N; i++)
    {
        C[i] = A[i] + B[i];
    }

    timeit_stop(&time_loop);
    printf("The loop took %ld microseconds.\n",time_loop.time_spent);

    free(A);
    free(B);
    free(C);

    return 0;
}

