# A '#' indicates that the line was verified
############################################
This is a toy example created to test basic operation of timing tools
(statistical gate-level and Monte-Carlo) that is used as golden for TAU2013
statistical timing tool contest. The objective of this example is to test some
aspects of the contest in a simple enough manner. The netlist and the library
are described in the TAU 2013 contest format as specified in the contest
web-page: "https://sites.google.com/site/taucontest2013/".

This example has 4 inputs and 3 outputs. We have 3 gates in the design:
NAND2_X1 (NAND2), INV_X1 (INV) and BUF_X1 (BUF). Each of these gates are fed by
primary inputs and they each drive a primary output.
                      _________
               nx1_1  |       |
      nx1 >----------+|       | nx12_1
                      | NAND2 |+-------------< nx12
      nx2 >----------+|       |
                nx2_1 |_______|


                        ________
                  nx3_1 |      | nx33_1
       nx3 >-----------+| INV  |+-----------< nx33
                        |______|  RC data

                        ________
                  nx4_1 |      | nx44_1
       nx4 >-----------+| BUF  |+-----------< nx44
              RC data   |______|

NAND2: The purpose of this gate is to exercise merge point min/max calculation.
       The RC values chosen in all its nets are negligible and we will exercise
       non-metal variations included in the gate 'library'. The input slews to
       the gate are deterministic. Uses cell NAND2_X1.
       
INV:   The input wire has negligible RC, and therefore doesn't contribute to the
       slew or delay. The output wire: "nx33_1 ----> nx33" has significant RC
       value; we imagine that we are driving a long wire. The input slew to the
       wire is statistical, and the delay/slew calculation on the wire is
       non-trivial. Uses cell INV_X1.

BUF:   The input wire: "nx4 ----> nx4_1" has substantial RC value. Output slew @ the
       wire sink (gate input) is statistical and is a non-trivial calculation.
       The BUF gate sees statistical delay and slew as input. Uses cell BUF_X1.

-----------------------------------------------------------------------------------
Timing calculations
-----------------------------------------------------------------------------------
Convention: rise/fall/early/late --> R/F/E/L; RFEL means all of these combinations
Ref document:  tau2013_contest_rules.v1.2.pdf (in web-page)
Ref library:   tau2013_contest_library.v2.0.lb

#------------------------------------------------------
#NAND2:
#------------------------------------------------------
 Arrival times @ PI as well as gate inputs ~= 0
 Slews @ PI as well as gate inputs are (negligible wire delay) --
 nx1: 5e-12 for all RFEL
 nx2: 15e-12 for Fall EL and 10e-12 for Rise EL
 
Total downstream cap seen by gate: 2e-18
  wire nx12_1 nx12
    cap nx12_1 1e-18
    cap nx12 1e-18
    res nx12_1 nx12 0.01

Metal scale factors from library
 metal 0 1.0 1.0
 metal 3 0.85 1.24

Cap @ 0  sigma corner for metal parameter M = 2e-18                 from Eqn (26)
Cap @ +3 sigma corner for metal parameter M = 1.24*2e-18 = 2.48e-18 from Eqn (27)
Statistical cap = 2e-18 + (0.16e-18)M

cell NAND2_X1
  pin A1 input 1.5292e-15 1.59903e-15
  pin A2 input 1.50228e-15 1.6642e-15
  pin ZN output
#  timing A1 ZN negative_unate \
    2.34835e-12  1446.09 0.246223 -0.122016375 0.132337125 0.00901526 -0.0230948 0.0230665 0.0124819 \ slew-out fall
    1.00106e-12  1935.37 0.183302 -0.104161443 0.157110384 0.0168532  -0.0143184 0.0041709 0.0195853 \ slew-out rise
    3.94959e-12  2294.19 0.354266 -0.120260625 0.15485975  0.00792211 -0.0220732 0.0111095 0.0163953 \ dly rise to fall
    2.25311e-12  2496.42 0.531953 -0.124644375 0.12632625  0.00813015 -0.0185769 0.0192602 0.0230023 \ dly fall to rise

# Delay (RF) from nx1_1 (pin A1) ---> nx12_1 (pin ZN) : Eqn (23), (24) in doc
#    3.94959e-12 * (1 +  -0.120260625V + 0.15485975T + 0.00792211L + -0.0220732W + 0.0111095H + 0.0163953R)
#     + 2294.19 * (2e-18 + 0.16e-18M) + 0.354266*5e-12

 Delay (FR) from nx1_1 (pin A1) ---> nx12_1 (pin ZN) : Eqn (23), (24) in doc
    2.25311e-12 * (1 + -0.124644375V +  0.12632625T + 0.00813015L + -0.0185769W + 0.0192602H + 0.0230023R)
     + 2496.42 * (2e-18 + 0.16e-18M) + 0.531953*5e-12

# and similarly for slew_out ....

#--------------------
#Final values:
#--------------------
#   Mean		M sens		V sens		T sens		L sens		W sens		H sens		R sens
#3.58E-12	2.31374E-16	-2.86537E-13	3.10774E-13	2.1171E-14	-5.42347E-14	5.41682E-14	2.93119E-14	Slewout fall
1.92144E-12	3.09659E-16	-1.04272E-13	1.57277E-13	1.68711E-14	-1.43336E-14	4.17535E-15	1.96061E-14	Slewout rise
#5.72551E-12	3.6707E-16	-4.7498E-13	6.11633E-13	3.12891E-14	-8.71801E-14	4.3878E-14	6.47547E-14	Delay fall at NAND2 ouput
4.91787E-12	3.99427E-16	-2.80837E-13	2.84627E-13	1.83181E-14	-4.18558E-14	4.33953E-14	5.18267E-14	Delay rise at NAND2 output

#  timing A2 ZN negative_unate \
    1.28385e-12 1614.02 0.115363 -0.15766245  0.1095316875 0.0181644  -0.0241892  0.0123824 0.00707087 \
    2.43262e-12 1961.38 0.172059 -0.1112175   0.165069375  0.00750686 -0.00256708 0.0072239 0.00875519 \
    6.73873e-12 2236.18 0.27167  -0.140788125 0.11851515   0.0131515  -0.0202698  0.0226274 0.012878   \
    4.76409e-12 2470.96 0.539663 -0.129301875 0.132883125  0.00381033 -0.00506968 0.0158586 0.0128256

# Delay (RF) from nx2_1 (pin A2) ---> nx12_1 (pin ZN) : Eqn (23), (24) in doc
#    6.73873e-12 * (1 + -0.140788125V + 0.11851515T + 0.0131515L + -0.0202698W + 0.0226274H + 0.012878R)
#     +  2236.18 * (2e-18 + 0.16e-18M) + 0.27167*10e-12

 Delay (FR) from nx2_1 (pin A2) ---> nx12_1 (pin ZN) : Eqn (23), (24) in doc
    4.76409e-12 * (1 + -0.129301875V + 0.132883125T + 0.00381033L + -0.00506968W + 0.0158586H + 0.0128256R)
     + 2470.96 * (2e-18 + 0.16e-18M) + 0.539663*15e-12

# and similarly for slew_out ....

#--------------------
#Final values:
#--------------------
#   Mean		M sens		V sens		T sens		L sens		W sens		H sens		R sens
#2.44071E-12	2.58243E-16	-2.02415E-13	1.40622E-13	2.33204E-14	-3.10553E-14	1.58971E-14	9.07794E-15	Slewout fall
5.01743E-12	3.13821E-16	-2.7055E-13	4.01551E-13	1.82613E-14	-6.24473E-15	1.7573E-14	2.12981E-14	Slewout rise
###(difference in Metal)###9.4599E-12	3.57789E-16	-9.48733E-13	7.98642E-13	8.86244E-14	-1.36593E-13	1.5248E-13	8.67814E-14	Delay fall at NAND2 ouput
1.2864E-11	3.95354E-16	-6.16006E-13	6.33067E-13	1.81528E-14	-2.41524E-14	7.55518E-14	6.11023E-14	Delay rise at NAND2 output


 Early R arrival time @ nx12_1 and nx12 
  = min{ 
4.91787E-12	3.99427E-16	-2.80837E-13	2.84627E-13	1.83181E-14	-4.18558E-14	4.33953E-14	5.18267E-14
1.2864E-11	3.95354E-16	-6.16006E-13	6.33067E-13	1.81528E-14	-2.41524E-14	7.55518E-14	6.11023E-14
 }
= 4.91787E-12	3.99427E-16	-2.80837E-13	2.84627E-13	1.83181E-14	-4.18558E-14	4.33953E-14	5.18267E-14

 Early F arrival time @ nx12_1 and nx12 
  = min{ 
5.72551E-12	3.6707E-16	-4.7498E-13	6.11633E-13	3.12891E-14	-8.71801E-14	4.3878E-14	6.47547E-14
9.4599E-12	3.57789E-16	-9.48733E-13	7.98642E-13	8.86244E-14	-1.36593E-13	1.5248E-13	8.67814E-14
  }
= 5.72551E-12	3.6707E-16	-4.7498E-13	6.11633E-13	3.12891E-14	-8.71801E-14	4.3878E-14	6.47547E-14

 Late R arrival time @ nx12_1 and nx12 
  = max{ 
4.91787E-12	3.99427E-16	-2.80837E-13	2.84627E-13	1.83181E-14	-4.18558E-14	4.33953E-14	5.18267E-14
1.2864E-11	3.95354E-16	-6.16006E-13	6.33067E-13	1.81528E-14	-2.41524E-14	7.55518E-14	6.11023E-14
  }
= 1.2864E-11	3.95354E-16	-6.16006E-13	6.33067E-13	1.81528E-14	-2.41524E-14	7.55518E-14	6.11023E-14

# Late F arrival time @ nx12_1 and nx12 
#  = max{ 
#5.72551E-12	3.6707E-16	-4.7498E-13	6.11633E-13	3.12891E-14	-8.71801E-14	4.3878E-14	6.47547E-14
#(difference in Metal)#9.4599E-12	3.57789E-16	-9.48733E-13	7.98642E-13	8.86244E-14	-1.36593E-13	1.5248E-13	8.67814E-14
#  }
#= 9.4599E-12	3.57789E-16	-9.48733E-13	7.98642E-13	8.86244E-14	-1.36593E-13	1.5248E-13	8.67814E-14

# and similarly for slews ...

--------------------
Final values of arrival time and slews at nx12 (Early mode)
--------------------
2.44071E-12	2.58243E-16	-2.02415E-13	1.40622E-13	2.33204E-14	-3.10553E-14	1.58971E-14	9.07794E-15	nx12 FE slew
1.92144E-12	3.09659E-16	-1.04272E-13	1.57277E-13	1.68711E-14	-1.43336E-14	4.17535E-15	1.96061E-14	nx12 RE slew
5.72551E-12	3.6707E-16	-4.7498E-13	6.11633E-13	3.12891E-14	-8.71801E-14	4.3878E-14	6.47547E-14	nx12 at FE
4.91787E-12	3.99427E-16	-2.80837E-13	2.84627E-13	1.83181E-14	-4.18558E-14	4.33953E-14	5.18267E-14	nx12 at RE

#--------------------
#Final values of arrival time and slews at nx12 (Late mode)
#--------------------
#3.58236E-12	2.31374E-16	-2.86537E-13	3.10774E-13	2.1171E-14	-5.42347E-14	5.41682E-14	2.93119E-14	nx12 FL slew
5.01743E-12	3.13821E-16	-2.7055E-13	4.01551E-13	1.82613E-14	-6.24473E-15	1.7573E-14	2.12981E-14	nx12 RL slew
###(difference in Metal)###9.4599E-12	3.57789E-16	-9.48733E-13	7.98642E-13	8.86244E-14	-1.36593E-13	1.5248E-13	8.67814E-14	nx12 at FL
1.2864E-11	3.95354E-16	-6.16006E-13	6.33067E-13	1.81528E-14	-2.41524E-14	7.55518E-14	6.11023E-14	nx12 at RL

************************************************************************************************
Projected arrival time and slew values @ nx12
************************************************************************************************
Notes			Mean		Sigma	      WC proj (early)	WC proj (late)
nx12 FE slew		2.44071E-12	2.5018E-13	1.66266E-12
nx12 RE slew		1.92144E-12	1.91051E-13	1.29157E-12
nx12 at FE		5.72551E-12	7.83836E-13	3.18667E-12
nx12 at RE		4.91787E-12	4.08092E-13	3.54683E-12

nx12 FL slew		3.58236E-12	4.31123E-13	           	4.96136E-12
nx12 RL slew		5.01743E-12	4.85361E-13	           	6.53694E-12
nx12 at FL		9.4599E-12	1.26302E-12	           	1.35014E-11
nx12 at RL		1.2864E-11	8.89152E-13	           	1.57096E-11

#
#------------------------------------------------------
#BUF
#------------------------------------------------------
 Arrival times @ PI nx4 input ~= 0
 Slews @ PI = 5ps (for all RFEL)

Wire parasitics ---> 
wire nx4 nx4_1
    cap nx4 5e-15
    cap nx4_1 5e-15
    res nx4 nx4_1 200

Pin capacitance at BUF_X1 input:    8.7525e-16  (Fall)
                                    9.74659e-16 (Rise)
At Metal 0 sigma,
         200
   |---/\/\/\/\------|
   |                 |
  --- 5e-15        ---- (5e-15 + 9.74659e-16) = 5.974659e-15 ---> Delay = 200 * 5.974659e-15 = 1.1949318e-12  // Rise
  ---              ---- (5e-15 + 8.7525e-16)  = 5.87525e-15  ---> Delay = 200 * 5.87525e-15  = 1.17505e-12    // Fall
   |                 |
   |                 |
 ------           -------
 / / /             / / /

 
 At Metal +3 sigma, Res scale factor = 0.85, cap factor = 1.24
      200*0.85 = 170
   |---/\/\/\/\------|
   |                 |
  --- 6.2e-15      ---- (6.2e-15 + 9.74659e-16) = 7.174659e-15 ---> Delay = 170 * 7.174659e-15 = 1.21969203e-12 // Rise
  ---              ---- (6.2e-15 + 8.7525e-16)  = 7.07525e-15  ---> Delay = 170 * 7.07525e-15  = 1.2027925e-12  // Fall
   |                 |
   |                 |
 ------           -------
 / / /             / / /
   
# Wire delay from "nx4 ---> nx4_1"
# -------------------------------------------
 RISE:  1.1949318e-12 + (0.00825341e-12)M
# FALL:  1.17505e-12   + (0.0092475e-12)M
# -------------------------------------------

Output Slew calulations for wire:
Computation for BETA_0 (B_0) -->

At Metal 0 sigma,
         200
   |---/\/\/\/\------|
   |                 |
  ---              ---- 5.974659e-15 * 1.1949318e-12 ---> Rise: B_0 = 200 * 5.974659e-15 * 1.1949318e-12 = 1.42786200665124e-24
  ---              ---- 5.87525e-15  * 1.17505e-12   ---> Fall: B_0 = 200 * 5.87525e-15  * 1.17505e-12   = 1.3807425025e-24
   |                 |
   |                 |
 ------           -------
 / / /             / / /

 
 At Metal +3 sigma, Res scale factor = 0.85, cap factor = 1.24
      200*0.85 = 170
   |---/\/\/\/\------|
   |                 |
  ---              ---- 7.174659e-15 * 1.21969203e-12 ---> Rise: B_0 = 170 * 7.174659e-15 * 1.21969203e-12 = 1.4876486480455209e-24
  ---              ---- 7.07525e-15  * 1.2027925e-12  ---> Fall: B_0 = 170 * 7.07525e-15  * 1.2027925e-12  = 1.44670979805625e-24
   |                 |
   |                 |
 ------           -------
 / / /             / / /

 Computation for sHat_0 ---> = sqrt(2B_0 - delay^2)
   			@ Metal 0 sigma					@ Metal 3 sigma
   RISE:   sqrt(2*1.42786200665124e-24 - 1.1949318e-12^2)	sqrt(2*1.4876486480455209e-24 -  1.21969203e-12^2)
          = 1.1949318e-12					= 1.21969203e-12

   FALL:   sqrt(2*1.3807425025e-24 - 1.17505e-12^2)		sqrt(2*1.44670979805625e-24 - 1.2027925e-12^2)
          =   1.17505e-12					= 1.2027925e-12

 Parametric sHat_0 -->
 RISE: 1.1949318e-12 + (0.0082534e-12)M
 FALL: 1.17505e-12   + (0.0092475e-12)M

 Computation of final parametric slew at wire tap node --
 
 Output slew s0 at metal sigma = 0:
 RISE = sqrt(5e-12^2 + 1.1949318e-12^2) = 5.14080363e-12
 FALL = sqrt(5e-12^2 + 1.17505e-12^2)   = 5.1362187e-12

 Output slew s0 at metal sigma = 3:
 RISE = sqrt(5e-12^2 + 1.21969203e-12^2) = 5.14661526131e-12
 FALL = sqrt(5e-12^2 + 1.2027925e-12^2)  = 5.1426364637e-12

# Paramertic final wire output slew:
# -------------------------------------------
 RISE: 5.14080363e-12 + (0.0019372e-12)M
# FALL: 5.1362187e-12  + (0.0021392e-12)M
# -------------------------------------------

 Next, cell delay computation --
 From library
cell BUF_X1
  pin A input 8.7525e-16 9.74659e-16
  pin Z output
  timing A Z positive_unate \
    3.55265e-12 986.161 0.015554 -0.11424275 0.167051 0.0057496 -0.0144676 0.0123458 0.0084767 \
    2.87048e-12 2275.22 0.002675 -0.13177404 0.125906 0.0175668 -0.0135049 0.0155747 0.0188234 \
    3.44931e-11 1234.45 0.283032 -0.115834   0.178225 0.0140265 -0.0025610 0.0035806 0.0029896 \
    2.67139e-11 2395.68 0.039533 -0.1574298  0.111035 0.0181536 -0.0186659 0.0117669 0.00529011

#--------------------
#Final delay and slew values at BUF output:
#--------------------
#   Mean		M sens		V sens		T sens		L sens		W sens		H sens		R sens
#3.63451E-12	1.57786E-16	-4.05865E-13	5.93474E-13	2.04263E-14	-5.13983E-14	4.38603E-14	3.0115E-14	Slewout fall at BUF output
2.88879E-12	3.64035E-16	-3.78255E-13	3.61411E-13	5.04251E-14	-3.87655E-14	4.47069E-14	5.40322E-14	Slewout rise at BUF output
###(Differences in all quantities)### 3.59493E-11	1.97512E-16	-3.99547E-12	6.14753E-12	4.83817E-13	-8.83392E-14	1.23506E-13	1.03121E-13	Delay fall at BUF output
### Think it can be ignored as what they could have meant by 'Final delay' is just the combo cells delay and not the arrival time
### Because, the arrival time below seems to match exactly.
2.69219E-11	3.83309E-16	-4.20556E-12	2.96618E-12	4.84953E-13	-4.98639E-13	3.1434E-13	1.41319E-13	Delay rise at BUF output

#--------------------
#Final arrival time values @ nx4:
#--------------------
#   Mean		M sens		V sens		T sens		L sens		W sens		H sens		R sens
#3.71243E-11	9.44501E-15	-3.99547E-12	6.14753E-12	4.83817E-13	-8.83392E-14	1.23506E-13	1.03121E-13	Fall at @ nx44
2.81169E-11	8.63672E-15	-4.20556E-12	2.96618E-12	4.84953E-13	-4.98639E-13	3.1434E-13	1.41319E-13	Rise at @ nx44

************************************************************************************************
Projected arrival time and slew values @ nx44
************************************************************************************************
			  Mean		Sigma	      WC proj (early)	WC proj (late)
Slewout fall @ nx44	3.63451E-12	7.23068E-13	1.37637E-12	5.89265E-12
Slewout rise @ nx44	2.88879E-12	5.31656E-13	1.13889E-12	4.63868E-12
Fall at @ nx44		3.71243E-11	7.35009E-12	1.47386E-11	5.95101E-11
Rise at @ nx44		2.81169E-11	5.20458E-12	1.2059E-11	4.41747E-11
#
#------------------------------------------------------
#INV:
#------------------------------------------------------
 Arrival times @ PI as well as gate inputs ~= 0
 Slews @ PI as well as gate inputs are 5ps (for all RFEL) since wire delay negligible
 
Total downstream cap seen by gate: 20e-15
wire nx33_1 nx33
    cap nx33_1 10e-15
    cap nx33 10e-15
    res nx33_1 nx33 500

Metal scale factors from library
 metal 0 1.0 1.0
 metal 3 0.85 1.24

Cap @ 0  sigma corner for metal parameter M = 20e-15                 from Eqn (26)
Cap @ +3 sigma corner for metal parameter M = 1.24*20e-15 = 24.8e-15 from Eqn (27)
#Statistical cap = 20e-15 + (1.6e-15)M

cell INV_X1
  pin A input 1.54936e-15 1.70023e-15
  pin ZN output
  timing A ZN negative_unate \
    8.63923e-13 790.188 0.255437 -0.14707425 0.112763125 0.0195758 -0.0183035 0.0010319 0.0199619  \
    1.40561e-13 1944.94 0.177376 -0.10392932 0.159019453 0.0168004 -0.02222   0.0189393 0.00961348 \
    3.97831e-12 1633.47 0.237001 -0.11110625 0.0694887   0.0207528 -0.0190257 0.011222  0.00813796 \
    3.79839e-13 2486.52 0.541413 -0.17650898 0.102166015 0.0023730 -0.0249535 0.0059718 0.00395789
 
#--------------------
#Final values at cell output:
#--------------------
#   Mean		M sens		V sens		T sens		L sens		W sens		H sens		R sens
#1.79449E-11	1.2643E-12	-1.27061E-13	9.74187E-14	1.6912E-14	-1.58128E-14	8.91482E-16	1.72455E-14	Slewout fall
3.99262E-11	3.1119E-12	-1.46084E-14	2.23519E-14	2.36148E-15	-3.12327E-15	2.66213E-15	1.35128E-15	Slewout rise
#3.78327E-11	2.61355E-12	-4.42015E-13	2.76448E-13	8.25611E-14	-7.56901E-14	4.46446E-14	3.23753E-14	Delay fall at INV output
5.28173E-11	3.97843E-12	-6.7045E-14	3.88066E-14	9.01385E-16	-9.47831E-15	2.26833E-15	1.50336E-15	Delay rise at INV output
#
#Next, wire delay for "nx33_1 ----> nx33" segment based on eqn (11)

At Metal 0 sigma,
         500
   |---/\/\/\/\------|
   |                 |
  --- 10e-15       ---- 10e-15   Delay = 500 * 10e-15 = 5e-12
  ---              ----
   |                 |
   |                 |
 ------           -------
 / / /             / / /

 At Metal +3 sigma, Res scale factor = 0.85, cap factor = 1.24
      500*0.85 = 425
   |---/\/\/\/\------|
   |                 |
  --- 12.4e-15     ---- 12.4e-15   Delay = 425 * 12.4e-15 = 5.27e-12
  ---              ----
   |                 |
   |                 |
 ------           -------
 / / /             / / /
   
 Statistical wire delay (from eqn 13 and beyond) = 5e-12 + (0.09e-12)M (for all RFEL)
 
 Statistical slew at nx33:
 Beta	
 Mean		M sens
 2.5E-23	9.243E-25	RFEL

 s_hat	
 Mean	M sens
 5E-12	9E-14
 5E-12	9E-14

#--------------------
# Final output slew at nx33:
#--------------------
# Mean		M sens		V sens		T sens		L sens		W sens		H sens		R sens
###(difference in R)### 1.86284E-11	1.24635E-12	-1.22397E-13	9.38431E-14	1.62914E-14	-1.52326E-14	8.5877E-16	1.66127E-14 / Fall
# 4.02381E-11	3.10165E-12	-1.44952E-14	2.21787E-14	2.34318E-15	-3.09906E-15	2.64149E-15	1.34081E-15 / Rise

 Final arrival times at primary output nx33
 RISE (both early and late) =
   Mean		M sens		V sens		T sens		L sens		W sens		H sens		R sens
SUM {
5.28173E-11	3.97843E-12	-6.7045E-14	3.88066E-14	9.01385E-16	-9.47831E-15	2.26833E-15	1.50336E-15   +
  5E-12         0.09E-12
}
-------------------------------------------------------------------------------------------------------------------------------
= 5.78173E-11	4.06843E-12	-6.7045E-14	3.88066E-14	9.01385E-16	-9.47831E-15	2.26833E-15	1.50336E-15	Rise at @ nx33
#
# Final arrival times at primary output nx33
# FALL (both early and late) =
#   Mean		M sens		V sens		T sens		L sens		W sens		H sens		R sens
#SUM {
#3.78327E-11	2.61355E-12	-4.42015E-13	2.76448E-13	8.25611E-14	-7.56901E-14	4.46446E-14	3.23753E-14 +
#  5E-12         0.09E-12
#}
#-------------------------------------------------------------------------------------------------------------------------------
#= 4.28327E-11	2.70355E-12	-4.42015E-13	2.76448E-13	8.25611E-14	-7.56901E-14	4.46446E-14	3.23753E-14	Fall at @ nx33
#

************************************************************************************************
Projected values of the arrival times and slews @ nx33
************************************************************************************************
			  Mean		Sigma	      WC proj (early)	WC proj (late)
Fall slew @ nx33	1.86284E-11	1.25616E-12	1.4372E-11	2.28848E-11
Rise slew @ nx33	4.02381E-11	3.10176E-12	3.08484E-11	4.96278E-11
Fall at @ nx33		4.28327E-11	2.75619E-12	3.30196E-11	5.26458E-11
Rise at @ nx33		5.78173E-11	4.06918E-12	4.53733E-11	7.02614E-11
************************************************************************************************

Early mode rat   @ nx33 (from netlist file) = 5e-12 (fall), 2e-12 (rise)
Early mode slack @ nx33        
  (FALL)  = "fall arrival time" - 5e-12
  	  = 3.78327E-11		2.70355E-12	-4.42015E-13	2.76448E-13	8.25611E-14	-7.56901E-14	4.46446E-14	3.23753E-14

  (RISE)  = "rise arrival time" - 2e-12
	  = 5.58173E-11		4.06843E-12	-6.7045E-14	3.88066E-14	9.01385E-16	-9.47831E-15	2.26833E-15	1.50336E-15

************************************************************************************************
Projected values of the early mode slack @ nx33, nx33_1 (rise and fall reversed for nx3 and nx3_1)
(Slack along the path remains same)
************************************************************************************************
  				Mean		Sigma	      WC proj (early)
Early Fall slack @ nx33		3.78327E-11	2.75619E-12	2.80196E-11
Early Rise slack @ nx33		5.58173E-11	4.06918E-12	4.33733E-11
************************************************************************************************

Late mode rat   @ nx33 (from netlist file) = 10e-12
Late mode slack @ nx33        
  (FALL)  = 10e-12 - "fall arrival time"
  	  = -3.28327E-11	-2.70355E-12	4.42015E-13	-2.76448E-13	-8.25611E-14	7.56901E-14	-4.46446E-14	3.23753E-14

  (RISE)  = 10e-12 - "rise arrival time"
	  = -4.78173E-11	-4.06843E-12	6.7045E-14	-3.88066E-14	-9.01385E-16	9.47831E-15	-2.26833E-15	1.50336E-15

************************************************************************************************
Projected values of the late mode slack @ nx33, nx33_1 (rise and fall reversed for nx3 and nx3_1)
(Slack along the path remains same)
************************************************************************************************
  				Mean		Sigma	      WC proj (early)
Late Fall slack @ nx33		-3.28327E-11	2.75619E-12	-4.26458E-11
Late Rise slack @ nx33		-4.78173E-11	4.06918E-12	-6.02614E-11
************************************************************************************************
