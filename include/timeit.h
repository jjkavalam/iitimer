/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#ifndef _TIMEIT_H_
#define _TIMEIT_H_

#include <time.h>
#include <sys/time.h>
typedef struct
{
    struct timeval begin, end;
    long time_spent;
    
} TimeIt;

void timeit_start(TimeIt *myclock);
void timeit_stop(TimeIt *myclock);

#endif
