/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#ifndef _PINS_H_
#define _PINS_H_

/*
 * A pin should have an id which is unique to it.
 * Moreover,
 * A signal always travels from a pin to another during which it gets transformed
 * The transfer function is defined by the pair of pins between which the signal travels
 * H = f ( pinA, pinB )
 * Thus we say that each pin contributes to half of the transfer function
 * This 'half transfer function' should be inferable from the pin.
 * The pin *may* also carry some information about some signal passing through it.
 *
 *  Example
 *  typedef struct {
 *
 *      // unique id and the
 *      char nodename[65];
 *
 *      // For the input pin of a gate
 *      // the half transfer function is inferable from
 *      // the knowledge of the (celltype, pinid)
 *      // the knowledge of (ins)
 *      int celltype;
 *      int pinid;
 *   } instanceIPin;
 *
*/
typedef struct{
	/* 0 -mean,
	 * 1 - voltage, 2 - temp. 3 -threshold
	 * 4 - L, 5 - W
	 * 6 - metal
	 * 7 - random component */
	int slack_early, slack_late;	// Currently used in parser_netlist only.
    float atRiseLate[8], atFallLate[8], atRiseEarly[8], atFallEarly[8];
    float slewRiseLate[8], slewFallLate[8], slewRiseEarly[8], slewFallEarly[8];
    float ratRiseLate[8], ratFallLate[8], ratRiseEarly[8], ratFallEarly[8];
} NODE_TIMING_DATA;

struct _instanceIPin {

    char nodename[65];
    int pinid;
    int isclock;
    NODE_TIMING_DATA tData;

    struct _Instance *instance;

    // Links
    struct _instanceOPin *iop;
    struct _wireTap *wt;

    struct _instanceIPin *next;
};
typedef struct _instanceIPin instanceIPin;

struct _instanceOPin{
    char nodename[65];
    int pinid;
    NODE_TIMING_DATA tData;

    float fallCap[2], riseCap[2];
    // stores the fallCap and riseCap as
    // seen from this particular output pin
    struct _Instance *instance;

    // Links
    struct _wirePort *wp;
    struct _instanceIPin *iip;

    struct _instanceOPin *next;
};
typedef struct _instanceOPin instanceOPin;

struct _wireTap{
    char nodename[65];
    struct _Wire *wire;
    int tapid;

    // Links
    struct _instanceIPin *iip;


    struct _wireTap *next;
};
typedef struct _wireTap wireTap;

struct _wirePort {
    char nodename[65];
    struct _Wire *wire;

    // Links
    struct _instanceOPin *iop;

    struct _wirePort *next;
};
typedef struct _wirePort wirePort;
/*
 *  Next container structures for these need to be thought of.
 *  Since the number of each pin is not known apriori, we go with a linked list.
 *  The next pointers are added to the above structures for this purpose.
 *
 *  Book keeping variables for the linked lists - head, curr - are defined below
 *  Template:
 *  ---------
 *  typedef struct
 *  {
 *      <node> *curr, *head;
 *      int count;
 *
 *  } <list>;
 *
 *  void init<list> (<list> *list);
 *  void free<list> (<list> *list);
 *  void add<node> (<node> *node, <list> *list);
 */


typedef struct
{
   instanceIPin *curr, *head;
   int count;

} IIPList;

void initIIPList (IIPList *list);
void freeIIPList (IIPList *list);
void addinstanceIPin (instanceIPin* node, IIPList* list);

typedef struct
{
    instanceOPin *curr, *head;
    int count;

} IOPList;

void initIOPList (IOPList *list);
void freeIOPList (IOPList *list);
void addinstanceOPin (instanceOPin *node, IOPList *list);

typedef struct
{
    wirePort *curr, *head;
    int count;

} WPList;

void initWPList (WPList *list);
void freeWPList (WPList *list);
void addwirePort (wirePort *node, WPList *list);

typedef struct
{
    wireTap *curr, *head;
    int count;

} WTList;

void initWTList (WTList *list);
void freeWTList (WTList *list);
void addwireTap (wireTap *node, WTList *list);
void printTData (NODE_TIMING_DATA, char *, int );
/*
 *  Declare the linked lists to global scope
 */
extern IIPList iipList, icpList;
extern IOPList iopList;
extern IOPList at_known_node_List;  // List of IOPs whose arrival times are known. Duplicates of these nodes are there in the main iopList also
extern IOPList slew_known_node_List;// Similarly
extern IIPList rat_known_node_List;// Similarly
extern WPList wpList;
extern WTList wtList;

extern float clockTP;
extern char *clockNode;

#endif
