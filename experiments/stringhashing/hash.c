/*
 * 
    Reference code from: http://stackoverflow.com/questions/98153/whats-the-best-hashing-algorithm-to-use-on-a-stl-string-when-using-hash-map
    unsigned int
    hash(
        const char* s,
        unsigned int seed = 0)
    {
        unsigned hash = seed;
        while (*s)
        {
            hash = hash * 101  +  *s++;
        }
        return hash;
    }
*/

/*
 *  Aim of the experiment:
 *  Check if the above hash function leads to collisions on data sets typically encountered in the netlist file.
 *
 *  Method:
 *  Extract only the node names from a sample netlist file using
 *  scripts/checkForGateToGate.py which dumps the node list to a file
 *
 *  Hash each of the node name.
 *  Check for duplicates in the list of hash values generated and thus report collisions.
 *
 *  Results:
 *  No hash collisions were found on any of the benchmarks.
 *
 *  Inference:
 *  This '101' hash to be incorporated for the searchNodeName routines as follows.
 *  During parsing, whenever a new node is found, it is hashed and the corresponding
 *  and the (nodename,hash) pair is stored at location i.
 *  i is incremented.
 *
 *  At the end of parsing, the hash table is sorted.
 *  The new ordering is stored in oldOrder array such that
 *  hash[i] -> nodename [oldOrder[i]]
 *
 *  Whenever, a search for node name is issued,
 *  the search strings hash value, val, is generated.
 *  A suitable search algorithm on *sorted array* is used to search for val in the hash table
 *  
 */

#include <stdio.h>
#include <string.h>

unsigned int
hash(
    const char* s,
    unsigned int seed)
{
    unsigned hash = seed;
    while (*s)
    {
        hash = hash * 101  +  *s++;
    }
    return hash;
}

void sort(unsigned int *array, unsigned int *index, int n)
{
  unsigned c, d, t, max, maxitem;
  for ( c = 0; c < n-1; c ++ )
  {
    max = array[0];
    for ( d = 1, maxitem = 0; d < n - c; d ++ )
    {
        if ( array[d] > max )
        {
            max = array[d];
            maxitem = d;
        }
    }
    t   = array[maxitem];
    array[maxitem] = array[n-c-1];
    array[n-c-1] = t;

    t          = index[maxitem];
    index[maxitem]   = index[n-c-1];
    index[n-c-1] = t;

  }

 
  printf("\nSorting done.");
 
  return;
}

#define BUFFER_SIZE 100
#define MAX_LINES 1000

void main(int argc, char **argv)
{
    if (argc <= 1)
    {
        printf( "\nUsage: program <node list file> <hash-out-file>\n" );
        return;
    }
    
    FILE *fin = fopen (argv[1], "r");
    FILE *fout;
    
    if (argc <= 2)
    {
        fout =  fopen ("out.hash", "w");
        printf ( "\nGenerated hashes to be written to out.hash");
    }
    else
    {
        fout =  fopen (argv[2], "w");
        printf ( "\nGenerated hashes to be written to %s",argv[2]);
    }
    
    if ( fin == NULL )
    {
        printf ( "\nFile I/O failed.\n");
        return;
    }
    
    char line [BUFFER_SIZE];
    char *word;
    char words [MAX_LINES][BUFFER_SIZE];
    int LineNum = 0;
    unsigned int hashs [MAX_LINES];
    unsigned int index [MAX_LINES];

    char seps[]   = "\n ";
    
    while (LineNum < MAX_LINES)
    {
        if (fgets(line, BUFFER_SIZE, fin) == NULL) break;
        word = strtok ( line, seps );
        strcpy ( words[LineNum], word );
        
        hashs[LineNum] = hash ( word , 0 );
        //printf ("\n[%d] %s -> %d", LineNum, word, hashs[LineNum]);
        //fprintf (fout,"%s -> %u\n", word, hashs[LineNum]);
        LineNum++;
    }    
    fclose (fin);
    fclose (fout);
    
    
    printf ( "\nSucessfully generated hash for %d words.\n", LineNum);
    printf ( "\nSearching for collisions");

    int i;        
    for ( i = 0; i < LineNum; i++ )
    {
        index[i] = i;
    }
    
    sort ( hashs, index, LineNum );
    
    int dupcount = 0;
    if ( hashs[1] != hashs[0] )
    {
        //printf ("\n%d (1 copies)",hashs[0]);
    }
    for ( i = 1; i < LineNum; i++ )
    {
        if ( hashs[i] == hashs[i-1] )
        {
           dupcount ++;
           printf ("\nCollision. %s with %s",words[index[i]],words[index[i-1]]);
        }
        else
        {
            //printf ("\n%d (%d copies)",hashs[i-1],dupcount+1);
            dupcount = 0;
        }
    }    
    //printf ("\n%d (%d copies)",hashs[i-1],dupcount+1);

    for ( i = 0; i < LineNum; i++ )
    {
        //printf ("\n%d -> %s -> %d",index[i],words[index[i]],hashs[i]);
    }

    printf ("\nBye\n");
}
